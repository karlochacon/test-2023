#!/bin/bash
mkfs -t ext4 -L DOCKER /dev/xvdb
mount /dev/xvdb /var/lib/docker
echo ECS_CLUSTER=locall >> /etc/ecs/ecs.config
echo ACCOUNTALIAS=metadata >> /etc/accountalias
touch /etc/rancherhost
touch /etc/rancherkey 

echo -e "[Unit]\nDescription=Var Lib Expansion Mount\n[Mount]\nWhat=/dev/disk/by-label/DOCKER\nWhere=/var/lib/docker\n[Install]\nRequiredBy = local-fs.target \n" >> /etc/systemd/system/var-lib-docker.mount

echo -e "[Unit]\nDescription=Relabel docker mount\nAfter=var-lib-docker.mount" >> /etc/systemd/system/docker-relabel.service

echo -e "[Unit]\nDescription=Login to dockerhub\nAfter=docker.service network-online.target \nRequires=docker.service network-online.target " >> /etc/systemd/system/docker-login.service

systemctl daemon-reload

systemctl enable var-lib-docker.mount
systemctl enable docker-relabel.service
systemctl enable docker-login.service

systemctl start var-lib-docker.mount
systemctl start docker-relabel.service
systemctl start docker-login.service
